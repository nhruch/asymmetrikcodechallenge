import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { FormsModule } from '@angular/forms';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule, MatIconRegistry  } from '@angular/material/icon';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatDialogModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule
  ],
  exports: [
    CommonModule,
    MatDialogModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
  ]
})

export class MaterialAggregateModule {
  constructor(public matIconRegistry: MatIconRegistry) {
  }

  static forRoot(): ModuleWithProviders<MaterialAggregateModule> {
    return {
      ngModule: MaterialAggregateModule,
      providers: [MatIconRegistry],
    };
  }
}
