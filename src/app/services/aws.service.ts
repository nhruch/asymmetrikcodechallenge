import { Injectable } from '@angular/core';
import { AnalyzeDocumentCommand, StartDocumentAnalysisCommand, TextractClient, FeatureType } from "@aws-sdk/client-textract";
import { Auth, Storage } from 'aws-amplify';
import amplify from '../../aws-exports'


@Injectable({
  providedIn: 'root'
})
export class AwsService {
  constructor() {}

  /**
   * Upload an image to S3
   *
   * @param imageFile File to upload
   * @param Key Key to use for upload
   * @returns Promise containing the PutResult
   */
   async uploadImage(imageFile: File, Key: string) {
    const content = await new Promise((resolve, reject) => {
      let fileReader: FileReader = new FileReader();
      fileReader.onloadend = function(content) {
        //resolve(btoa(content.target?.result?.toString() ?? ""))
        resolve(content.target?.result)
      }
      fileReader.onerror = reject
      fileReader.readAsArrayBuffer(imageFile)
    })

    if(content) {
      console.log("Content length: ", (content as ArrayBuffer).byteLength)
      return Storage.put(Key, (content as ArrayBuffer))
    }
    return
   }

   /**
    * Extract text from an uploaded S3 file
    *
    * @param Name Key of the file which will have Textract run on
    * @returns Promise with the textract output
    */
   async extractDocument(Name: string) {
    const credentials = await Auth.currentCredentials()
    console.log(credentials)

    const client = new TextractClient({ credentials, region: amplify.aws_user_files_s3_bucket_region });
    const params = {
      Document: {
        S3Object: {
          Bucket: amplify.aws_user_files_s3_bucket,
          Name: `public/${Name}`
        },
      },
      FeatureTypes: [
        FeatureType.FORMS,
        FeatureType.TABLES
      ]
    }

    return client.send(new AnalyzeDocumentCommand(params))
   }
}
