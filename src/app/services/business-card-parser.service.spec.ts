import { TestBed } from '@angular/core/testing';

import { BusinessCardParserService } from './business-card-parser.service';

describe('BusinessCardParserService', () => {
  let service: BusinessCardParserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BusinessCardParserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
