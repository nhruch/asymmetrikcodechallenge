import { Injectable } from '@angular/core';
import { Address, PhoneType, CreateBusinessCardInfoInput, PhoneNumberInput, AddressInput } from '../API.service';
import { companyHintSimilarity, websiteHintSimilarity, nameHintSimilarity } from '../misc/utility'


@Injectable({
  providedIn: 'root'
})
export class BusinessCardParserService {

  // Common website regex
  websitePattern: RegExp = new RegExp(/^(https?:\/\/)?((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-z\d%_.~+]*)*/)

  // Phone regex (with type extraction)
  phonePattern: RegExp = new RegExp(/(?:([wfocm]\w*)[^\w]?)?\s*(\+\d+)?\s*((?:\(?(?:\d+)\)?)(?:(?:[\s-\.]*\(?(?:\d+)\)?))+)(?:\s*[^\d]?(x\d+))?(?:\s[^\w]?([wfocm]\w*)[^\w]?)?(?!\S)/gi)

  // L2 address regex
  addressRegionPattern: RegExp = new RegExp(/((?:[a-z]{2,}\s*)+),\s*([\w]{2})\s*([\d-]+)/i)

  /**
   * Constructor
   */
  constructor() { }

  /**
   * The business card parser first looks to find an email address. From that email address, the parser will use the domain as a * company hint and the id as a name hint. From there, each line is scanned looking for elements of the business card.
   *
   * To determine the company and person's name, each line of the card is loaded into a fuzzy hash search engine (fuse.js). Then
   * each hint is broken out into several hints and formatted. For the name hint, remove any qualifiers (e.g. M.D., Jr.), split
   * on spaces, and add the resulting elements to an array of name hints including the joined name. For the company hint, remove
   * the top level domain before scanning. These resulting hints will then scanned against the business card lines looking for
   * strong confidence in similarity. This allows us to more confidently locate the company and person's name on a card. For
   * example:
   *
   * - 'ABC Tech, Inc' will show high confidence with 'abctech.com' but will fail on all other lines
   * - 'nhruch' will show high confidence with 'Nicholas Hruch'
   * For phone numbers, use a very complicated regular expression to identify the phone type, country code, number and extension.
   * The parser supports identifying multiple phone numbers.
   *
   * For address, start by looking for the "City, State Zip" line via a regular expression. Check to see if the location of any
   * match is greater than 1 which may indicate that the other portions of the address could be before it. Then look one line up
   * to see if the last character of the line ends in a number. If it does, then treat this as Line 2 and use the line above it
   * as Line 1. The exception to this is if the start of the line is a number which indicates that it could include a Line 1.
   * Parse accordingly.
   *
   * @param content Business card content to be parsed
   * @returns BusinessCardInfo GraphQL Input
   */
  parseCard(content: string): CreateBusinessCardInfoInput {
    const parsed = {} as CreateBusinessCardInfoInput

    // Split out all the lines
    const lines = content.split(/\r?\n/)

    // Extract email from lines
    parsed.email = this.extractEmail(lines) ?? ""

    // Generate our hints
    const email_parts = parsed.email?.split('@')
    const name_hint: Nullable<string> = email_parts[0] ?? null
    const company_hint: Nullable<string> = email_parts[1]?.split('.')[0] ?? null

    // Get website from company hint
    parsed.website = this.getWebsite(lines, company_hint)

    // Get company from company hint
    parsed.company = this.getCompany(lines, company_hint)

    // Get name from name hint
    parsed.name = this.getName(lines, name_hint)

    // Find all phone numbers
    parsed.phone = this.getPhoneNumbers(lines)

    // Get address details
    parsed.address = this.getAddress(lines) ?? {} as Address

    return parsed
  }

  /**
   * Extract an email address from the provided lines
   *
   * @param lines Lines to be scanned for an email address
   * @returns The extracted email address or null
   */
  private extractEmail(lines: string[]): Nullable<string> {
    let email: Nullable<string> = null
    for (let i = 0; i < lines.length && !email; ++i) {
      const line_elements = lines[i].split(/\s/)
      for (const line_element of line_elements) {
        if (line_element.includes("@")) {
          email = line_element.trim()
          lines.splice(i, 1)
          break
        }
      }
    }
    return email
  }

  /**
   * Extract a website from the provided lines
   *
   * @param lines Lines to be scanned for an email address
   * @param hint Website hint used to pull the highest confidence website
   * @returns Website string
   */
  private getWebsite(lines: string[], hint?: Nullable<string>): string {
    let websites: string[] = []

    // Find all strings that match the website pattern
    for(const line of lines) {
      console.log(line)
      const matches = line.match(this.websitePattern)
      console.log(matches)
      if (matches && matches[2]) {
        websites.push(matches[2])
      }

    }

    // If more than one website exists, check to see which has the highest
    // similarity to the provided hint.
    if(websites.length <= 1) {
      return websites[0] ?? ""
    } else if (hint) {
      // Find the most probable website URL. A value of 1 is the maximum output
      // for Fuse.js
      let lowestScore = 1.1
      let lowestWebsite = ""
      for (const website of websites) {
        // Get a similarity score and replace the current selected website if the
        // confidence is higher
        const score = websiteHintSimilarity(website, hint)
        if (score < lowestScore) {
          lowestWebsite = website
        }
      }
      return lowestWebsite
    } else {
      return ""
    }
  }

  /**
   * Extract the contacts name from the provided lines with a provided hint
   *
   * @param lines Lines to be scanned for a name
   * @param hint Name hint to use for confidence based selection
   * @returns The contacts name
   */
  private getName(lines: string[], hint: string): string {
    // For each line, check the similarity score between the line and the hint.
    let current_score: Number = 1.1
    let chosen = ""
    for(const line of lines) {
      // Get a similarity score and replace the current selected name if the
      // confidence is higher
      const score = nameHintSimilarity(line, hint)
      if (score < current_score) {
        chosen = line
        current_score = score
      }
    }
    return chosen
  }

  /**
   * Extract the company name from the provided lines with a provided hint
   *
   * @param lines Lines to be scanned for company name
   * @param hint Company hint to use for confidence based selection
   * @returns Company name
   */
  private getCompany(lines: string[], hint: string): string {
    let current_score: Number = 1.1
    let chosen = ""
    for(const line of lines) {
      // Get a similarity score and replace the current selected company name if the
      // confidence is higher
      const score = companyHintSimilarity(line, hint)
      if (score < current_score) {
        chosen = line
        current_score = score
      }
    }
    return chosen
  }

  /**
   * Extract all phone numbers from the provided lines. Attempt to identify the
   * type of each number too.
   *
   * @param lines Lines to be scanned for phone numbers
   * @returns An array of PhoneNumberInputs which includes the number, country code,
   *   optional extension and type.
   */
  private getPhoneNumbers(lines: string[]): PhoneNumberInput[] {
    let numbers: PhoneNumberInput[] = []
    for (const line of lines) {
      // Search for phone pattern matches
      const matches = line.matchAll(this.phonePattern)
      for (const match of matches) {
        // Check that the number portion is above 8 digits which is an international standard
        if (match[3]?.trim().length < 8)
          continue

        // Determine the phone type using characters that show up around the number. Examples:
        // (m) <number> -> MOBILE
        // Cell: <number> -> CELL
        // <number> (f) -> FAX
        let type: PhoneType = PhoneType.UNKNOWN

        // Our type will show up in either the 1st or 5th capture group
        const type_str = match[1] ?? match[5]
        if (type_str) {
          const ch = type_str.trim().charAt(0).toLocaleLowerCase()
          if (ch === 'c' || ch === 'm') {
            type = PhoneType.CELL
          } else if (ch === 'w' || ch === 'o') {
            type = PhoneType.WORK
          } else if (ch === 'f') {
            type = PhoneType.FAX
          } else if (ch === 'h') {
            type = PhoneType.HOME
          }
        }

        // Create a phone number and add it to the list
        numbers.push({
          type,
          number: match[3]?.trim() ?? "",
          countryCode: match[2]?.trim() ?? "",
          ext: match[4] ?? ""
        })
      }
    }
    return numbers
  }

  /**
   * Get the company address from the provided lines if one is provided.
   *
   * @param lines Lines to be scanned for address
   * @returns AddressInput or null
   */
  private getAddress(lines: string[]): Nullable<AddressInput> {

    const address: AddressInput = {
      line1: "",
      line2: "",
      city: "",
      state: "",
      zip: ""
    }

    for(let index = 0; index < lines.length; ++index) {

      // Look for a region pattern in the line
      const match = lines[index].match(this.addressRegionPattern)
      if(match) {
        address.city = match[1]
        address.state = match[2]
        address.zip = match[3]

        // Check if the match occurred above the 6th column. This could indicate
        // that other portions of the address exist on the same line.
        if (match.index && match.index > 6) {
          // Get the portion of the line that did not match and validate that it
          // could be an address line.
          const addressSubstr = lines[index].substring(0, match.index)
          const match2 = addressSubstr.match(/(?:[\w\d-\.]+\s?){2,}/)
          if (match2)
            address.line1 = match2[0]
        } else {
          // Our regional data started on column 0. Look at previous lines to find
          // L1 and L2 data.
          if(index - 1 > 0) {
            // If the line above ends with digits, it is likely an L2 line.
            if(lines[index - 1].match(/\d+\s*$/)) {
              // If the line can be broken by a character that is not valid for an
              // address, this could mean both L1 and L2 are on the same line.
              const parts = lines[index - 1].split(/[^\w\d\s.-]/)
              if (parts.length > 1) {
                // L1 and L2 are on the same line (most likely)
                address.line1 = parts[0]
                address.line2 = parts.pop()
              } else {
                // L2 is alone on this line with L1 above it
                address.line2 = lines[index - 1]
                if(index - 2 > 0)
                  address.line1 = lines[index - 2]
              }
            } else {
              // There is no L2
              address.line1 = lines[index - 1]
            }
          }
        }
        break
      }
    }
    return address
  }
}
