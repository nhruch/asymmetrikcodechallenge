export enum BusinessCardInputType {
  MANUAL,
  IMAGE
}

export interface ParsedBusinessCardDTO {
  type: BusinessCardInputType,
  content: string,
  image?: string
}
