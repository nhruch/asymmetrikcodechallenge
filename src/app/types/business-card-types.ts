export enum PhoneType {
  UNKNOWN,
  HOME,
  CELL,
  WORK,
  FAX
}

export interface Address {
  line1: string,
  line2?: string,
  city: string,
  state: string,
  zip: string
}

export interface PhoneNumber {
  type: PhoneType,
  countryCode: string,
  number: string,
  ext: string
}

export interface BusinessCardInfo {
  name?: string
  company?: string
  website?: string
  address?: Address
  phone?: PhoneNumber[]
  email?: string
  extra?: string
}
