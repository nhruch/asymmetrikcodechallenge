import Fuse from 'fuse.js'

const CompanyQualifiers: Set<string> = new Set<string>([
  "llc",
  "lp",
  "llp",
  "lllp",
  "pc",
  "pa",
  "limited",
  "ltd",
  "corporation",
  "co",
  "inc",
  "incorporated",
  "partnership",
  "lp",
])

const domainNameRegex: RegExp = new RegExp(/^(?:https?:\/\/)?(?:(?:([a-z\d](?:[a-z\d-]*[a-z\d])*)\.)+([a-z]{2,})|(?:(?:\d{1,3}\.){3}\d{1,3}))(?:\:\d+)?(?:\/[-a-z\d%_.~+]*)*/)

export function companyHintSimilarity(company: string, hint: string): Number {
  // Format the two inputs
  const target_tokens = company.replace(/[^\w\s]/, "").toLocaleLowerCase().split(/\s/)
  if (CompanyQualifiers.has(target_tokens[target_tokens.length - 1]))
    target_tokens.pop()
    company = target_tokens.join("")
  hint = hint.replace(/[^\w]/, "").toLocaleLowerCase()

  // Check similarity
  return hintSimilarity(company, hint)
}

export function websiteHintSimilarity(website: string, hint: string): Number {
  // Format the two inputs
  hint = hint.replace(/[^\w]/, "").toLocaleLowerCase()
  const match = domainNameRegex.exec(website)
  if (match && match.length >= 2) {
    return hintSimilarity(match["1"], hint)
  } else {
    return hintSimilarity(website, hint)
  }
}

export function nameHintSimilarity(name: string, hint: string): Number {
  // Format the two inputs
  const hint_parts = hint.toLocaleLowerCase().split(/[^\w]/)
  const hints = [
    ...hint_parts,
    hint_parts.join(" ")
  ]

  const name_parts = name.split(" ")
  const name_parts_no_titles = name_parts.filter(entry => !entry.search('.')).join(" ")

  const names = [
    name,
    name_parts_no_titles,
    ...name_parts
  ]

  console.log("Input: ", names, hints)
  const score = hintSimilarityMulti(names, hints)
  console.log("Score: ", score)
  return score
}

function hintSimilarity(target: string, hint: string, threshold: number = .6): Number {
  const targets = [{target}]
  const fuse = new Fuse(targets, {keys: ["target"], threshold, includeScore: true})
  const results = fuse.search(hint)
  return results[0]?.score ?? 1
}

function hintSimilarityMulti(targets: string[], hints: string[], threshold: number = .6): Number {
  const formattedTargets = []
  for (const target of targets) {
    formattedTargets.push({target})
  }
  const fuse = new Fuse(formattedTargets, {keys: ["target"], threshold, includeScore: true})
  const results = []
  for (const hint of hints) {
    const result = fuse.search(hint)
    if (result?.length) results.push(result)
  }

  let score = 1
  console.log(results)
  for (const result of results) {
    if (result[0]?.score && result[0].score < score)
      score = result[0].score
  }
  return score
}
