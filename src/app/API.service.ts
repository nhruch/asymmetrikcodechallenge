/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.
import { Injectable } from "@angular/core";
import API, { graphqlOperation, GraphQLResult } from "@aws-amplify/api-graphql";
import { Observable } from "zen-observable-ts";

export interface SubscriptionResponse<T> {
  value: GraphQLResult<T>;
}

export type __SubscriptionContainer = {
  onCreateBusinessCardInfo: OnCreateBusinessCardInfoSubscription;
  onUpdateBusinessCardInfo: OnUpdateBusinessCardInfoSubscription;
  onDeleteBusinessCardInfo: OnDeleteBusinessCardInfoSubscription;
};

export type CreateBusinessCardInfoInput = {
  id?: string | null;
  user: string;
  name?: string | null;
  company?: string | null;
  website?: string | null;
  address?: AddressInput | null;
  phone?: Array<PhoneNumberInput | null> | null;
  email?: string | null;
  extra?: string | null;
};

export type AddressInput = {
  line1?: string | null;
  line2?: string | null;
  city?: string | null;
  state?: string | null;
  zip?: string | null;
};

export type PhoneNumberInput = {
  type: PhoneType;
  countryCode?: string | null;
  number: string;
  ext?: string | null;
};

export enum PhoneType {
  UNKNOWN = "UNKNOWN",
  HOME = "HOME",
  CELL = "CELL",
  WORK = "WORK",
  FAX = "FAX"
}

export type ModelBusinessCardInfoConditionInput = {
  user?: ModelStringInput | null;
  name?: ModelStringInput | null;
  company?: ModelStringInput | null;
  website?: ModelStringInput | null;
  email?: ModelStringInput | null;
  extra?: ModelStringInput | null;
  and?: Array<ModelBusinessCardInfoConditionInput | null> | null;
  or?: Array<ModelBusinessCardInfoConditionInput | null> | null;
  not?: ModelBusinessCardInfoConditionInput | null;
};

export type ModelStringInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null"
}

export type ModelSizeInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
};

export type BusinessCardInfo = {
  __typename: "BusinessCardInfo";
  id: string;
  user: string;
  name?: string | null;
  company?: string | null;
  website?: string | null;
  address?: Address | null;
  phone?: Array<PhoneNumber | null> | null;
  email?: string | null;
  extra?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type Address = {
  __typename: "Address";
  line1?: string | null;
  line2?: string | null;
  city?: string | null;
  state?: string | null;
  zip?: string | null;
};

export type PhoneNumber = {
  __typename: "PhoneNumber";
  type: PhoneType;
  countryCode?: string | null;
  number: string;
  ext?: string | null;
};

export type UpdateBusinessCardInfoInput = {
  id: string;
  user?: string | null;
  name?: string | null;
  company?: string | null;
  website?: string | null;
  address?: AddressInput | null;
  phone?: Array<PhoneNumberInput | null> | null;
  email?: string | null;
  extra?: string | null;
};

export type DeleteBusinessCardInfoInput = {
  id: string;
};

export type ModelBusinessCardInfoFilterInput = {
  id?: ModelIDInput | null;
  user?: ModelStringInput | null;
  name?: ModelStringInput | null;
  company?: ModelStringInput | null;
  website?: ModelStringInput | null;
  email?: ModelStringInput | null;
  extra?: ModelStringInput | null;
  and?: Array<ModelBusinessCardInfoFilterInput | null> | null;
  or?: Array<ModelBusinessCardInfoFilterInput | null> | null;
  not?: ModelBusinessCardInfoFilterInput | null;
};

export type ModelIDInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export type ModelBusinessCardInfoConnection = {
  __typename: "ModelBusinessCardInfoConnection";
  items: Array<BusinessCardInfo | null>;
  nextToken?: string | null;
};

export type CreateBusinessCardInfoMutation = {
  __typename: "BusinessCardInfo";
  id: string;
  user: string;
  name?: string | null;
  company?: string | null;
  website?: string | null;
  address?: {
    __typename: "Address";
    line1?: string | null;
    line2?: string | null;
    city?: string | null;
    state?: string | null;
    zip?: string | null;
  } | null;
  phone?: Array<{
    __typename: "PhoneNumber";
    type: PhoneType;
    countryCode?: string | null;
    number: string;
    ext?: string | null;
  } | null> | null;
  email?: string | null;
  extra?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateBusinessCardInfoMutation = {
  __typename: "BusinessCardInfo";
  id: string;
  user: string;
  name?: string | null;
  company?: string | null;
  website?: string | null;
  address?: {
    __typename: "Address";
    line1?: string | null;
    line2?: string | null;
    city?: string | null;
    state?: string | null;
    zip?: string | null;
  } | null;
  phone?: Array<{
    __typename: "PhoneNumber";
    type: PhoneType;
    countryCode?: string | null;
    number: string;
    ext?: string | null;
  } | null> | null;
  email?: string | null;
  extra?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type DeleteBusinessCardInfoMutation = {
  __typename: "BusinessCardInfo";
  id: string;
  user: string;
  name?: string | null;
  company?: string | null;
  website?: string | null;
  address?: {
    __typename: "Address";
    line1?: string | null;
    line2?: string | null;
    city?: string | null;
    state?: string | null;
    zip?: string | null;
  } | null;
  phone?: Array<{
    __typename: "PhoneNumber";
    type: PhoneType;
    countryCode?: string | null;
    number: string;
    ext?: string | null;
  } | null> | null;
  email?: string | null;
  extra?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type GetBusinessCardInfoQuery = {
  __typename: "BusinessCardInfo";
  id: string;
  user: string;
  name?: string | null;
  company?: string | null;
  website?: string | null;
  address?: {
    __typename: "Address";
    line1?: string | null;
    line2?: string | null;
    city?: string | null;
    state?: string | null;
    zip?: string | null;
  } | null;
  phone?: Array<{
    __typename: "PhoneNumber";
    type: PhoneType;
    countryCode?: string | null;
    number: string;
    ext?: string | null;
  } | null> | null;
  email?: string | null;
  extra?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type ListBusinessCardInfosQuery = {
  __typename: "ModelBusinessCardInfoConnection";
  items: Array<{
    __typename: "BusinessCardInfo";
    id: string;
    user: string;
    name?: string | null;
    company?: string | null;
    website?: string | null;
    address?: {
      __typename: "Address";
      line1?: string | null;
      line2?: string | null;
      city?: string | null;
      state?: string | null;
      zip?: string | null;
    } | null;
    phone?: Array<{
      __typename: "PhoneNumber";
      type: PhoneType;
      countryCode?: string | null;
      number: string;
      ext?: string | null;
    } | null> | null;
    email?: string | null;
    extra?: string | null;
    createdAt: string;
    updatedAt: string;
  } | null>;
  nextToken?: string | null;
};

export type OnCreateBusinessCardInfoSubscription = {
  __typename: "BusinessCardInfo";
  id: string;
  user: string;
  name?: string | null;
  company?: string | null;
  website?: string | null;
  address?: {
    __typename: "Address";
    line1?: string | null;
    line2?: string | null;
    city?: string | null;
    state?: string | null;
    zip?: string | null;
  } | null;
  phone?: Array<{
    __typename: "PhoneNumber";
    type: PhoneType;
    countryCode?: string | null;
    number: string;
    ext?: string | null;
  } | null> | null;
  email?: string | null;
  extra?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateBusinessCardInfoSubscription = {
  __typename: "BusinessCardInfo";
  id: string;
  user: string;
  name?: string | null;
  company?: string | null;
  website?: string | null;
  address?: {
    __typename: "Address";
    line1?: string | null;
    line2?: string | null;
    city?: string | null;
    state?: string | null;
    zip?: string | null;
  } | null;
  phone?: Array<{
    __typename: "PhoneNumber";
    type: PhoneType;
    countryCode?: string | null;
    number: string;
    ext?: string | null;
  } | null> | null;
  email?: string | null;
  extra?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteBusinessCardInfoSubscription = {
  __typename: "BusinessCardInfo";
  id: string;
  user: string;
  name?: string | null;
  company?: string | null;
  website?: string | null;
  address?: {
    __typename: "Address";
    line1?: string | null;
    line2?: string | null;
    city?: string | null;
    state?: string | null;
    zip?: string | null;
  } | null;
  phone?: Array<{
    __typename: "PhoneNumber";
    type: PhoneType;
    countryCode?: string | null;
    number: string;
    ext?: string | null;
  } | null> | null;
  email?: string | null;
  extra?: string | null;
  createdAt: string;
  updatedAt: string;
};

@Injectable({
  providedIn: "root"
})
export class APIService {
  async CreateBusinessCardInfo(
    input: CreateBusinessCardInfoInput,
    condition?: ModelBusinessCardInfoConditionInput
  ): Promise<CreateBusinessCardInfoMutation> {
    const statement = `mutation CreateBusinessCardInfo($input: CreateBusinessCardInfoInput!, $condition: ModelBusinessCardInfoConditionInput) {
        createBusinessCardInfo(input: $input, condition: $condition) {
          __typename
          id
          user
          name
          company
          website
          address {
            __typename
            line1
            line2
            city
            state
            zip
          }
          phone {
            __typename
            type
            countryCode
            number
            ext
          }
          email
          extra
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateBusinessCardInfoMutation>response.data.createBusinessCardInfo;
  }
  async UpdateBusinessCardInfo(
    input: UpdateBusinessCardInfoInput,
    condition?: ModelBusinessCardInfoConditionInput
  ): Promise<UpdateBusinessCardInfoMutation> {
    const statement = `mutation UpdateBusinessCardInfo($input: UpdateBusinessCardInfoInput!, $condition: ModelBusinessCardInfoConditionInput) {
        updateBusinessCardInfo(input: $input, condition: $condition) {
          __typename
          id
          user
          name
          company
          website
          address {
            __typename
            line1
            line2
            city
            state
            zip
          }
          phone {
            __typename
            type
            countryCode
            number
            ext
          }
          email
          extra
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateBusinessCardInfoMutation>response.data.updateBusinessCardInfo;
  }
  async DeleteBusinessCardInfo(
    input: DeleteBusinessCardInfoInput,
    condition?: ModelBusinessCardInfoConditionInput
  ): Promise<DeleteBusinessCardInfoMutation> {
    const statement = `mutation DeleteBusinessCardInfo($input: DeleteBusinessCardInfoInput!, $condition: ModelBusinessCardInfoConditionInput) {
        deleteBusinessCardInfo(input: $input, condition: $condition) {
          __typename
          id
          user
          name
          company
          website
          address {
            __typename
            line1
            line2
            city
            state
            zip
          }
          phone {
            __typename
            type
            countryCode
            number
            ext
          }
          email
          extra
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteBusinessCardInfoMutation>response.data.deleteBusinessCardInfo;
  }
  async GetBusinessCardInfo(id: string): Promise<GetBusinessCardInfoQuery> {
    const statement = `query GetBusinessCardInfo($id: ID!) {
        getBusinessCardInfo(id: $id) {
          __typename
          id
          user
          name
          company
          website
          address {
            __typename
            line1
            line2
            city
            state
            zip
          }
          phone {
            __typename
            type
            countryCode
            number
            ext
          }
          email
          extra
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetBusinessCardInfoQuery>response.data.getBusinessCardInfo;
  }
  async ListBusinessCardInfos(
    filter?: ModelBusinessCardInfoFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListBusinessCardInfosQuery> {
    const statement = `query ListBusinessCardInfos($filter: ModelBusinessCardInfoFilterInput, $limit: Int, $nextToken: String) {
        listBusinessCardInfos(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            user
            name
            company
            website
            address {
              __typename
              line1
              line2
              city
              state
              zip
            }
            phone {
              __typename
              type
              countryCode
              number
              ext
            }
            email
            extra
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListBusinessCardInfosQuery>response.data.listBusinessCardInfos;
  }
  OnCreateBusinessCardInfoListener: Observable<
    SubscriptionResponse<
      Pick<__SubscriptionContainer, "onCreateBusinessCardInfo">
    >
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateBusinessCardInfo {
        onCreateBusinessCardInfo {
          __typename
          id
          user
          name
          company
          website
          address {
            __typename
            line1
            line2
            city
            state
            zip
          }
          phone {
            __typename
            type
            countryCode
            number
            ext
          }
          email
          extra
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<
    SubscriptionResponse<
      Pick<__SubscriptionContainer, "onCreateBusinessCardInfo">
    >
  >;

  OnUpdateBusinessCardInfoListener: Observable<
    SubscriptionResponse<
      Pick<__SubscriptionContainer, "onUpdateBusinessCardInfo">
    >
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateBusinessCardInfo {
        onUpdateBusinessCardInfo {
          __typename
          id
          user
          name
          company
          website
          address {
            __typename
            line1
            line2
            city
            state
            zip
          }
          phone {
            __typename
            type
            countryCode
            number
            ext
          }
          email
          extra
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<
    SubscriptionResponse<
      Pick<__SubscriptionContainer, "onUpdateBusinessCardInfo">
    >
  >;

  OnDeleteBusinessCardInfoListener: Observable<
    SubscriptionResponse<
      Pick<__SubscriptionContainer, "onDeleteBusinessCardInfo">
    >
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteBusinessCardInfo {
        onDeleteBusinessCardInfo {
          __typename
          id
          user
          name
          company
          website
          address {
            __typename
            line1
            line2
            city
            state
            zip
          }
          phone {
            __typename
            type
            countryCode
            number
            ext
          }
          email
          extra
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<
    SubscriptionResponse<
      Pick<__SubscriptionContainer, "onDeleteBusinessCardInfo">
    >
  >;
}
