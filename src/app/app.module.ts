import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AddCardDialogComponent } from './components/add-card-dialog/add-card-dialog.component';
import { AmplifyAuthenticatorModule } from '@aws-amplify/ui-angular';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialAggregateModule } from './material-aggregate.module';

@NgModule({
  declarations: [
    AppComponent,
    AddCardDialogComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AmplifyAuthenticatorModule,
    BrowserAnimationsModule,
    MaterialAggregateModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
