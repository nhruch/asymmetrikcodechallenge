import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BusinessCardParserService } from '../../services/business-card-parser.service';
import { ParsedBusinessCardDTO } from '../../types/parsed-business-card.dto';
import { AddCardDialogComponent } from '../add-card-dialog/add-card-dialog.component';
import { APIService, BusinessCardInfo, ListBusinessCardInfosQuery } from '../../API.service';
import { Auth } from 'aws-amplify';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  cards: BusinessCardInfo[] = []
  columns: string[] = [
    "name",
    "company",
    "email",
    "address",
    "phone",
    "website",
    "actions"
  ]

  constructor(
    private businessCardParser: BusinessCardParserService,
    private dialog: MatDialog,
    private api: APIService
  ) {
    // Pull the users contacts
    this.getContacts()
  }

  ngOnInit(): void {
  }

  /**
   * Launch the add card dialog so that a user can add a business card. Parse the
   * business card text and add it to the users storage.
   */
  async addCard() {
    const dialogRef = this.dialog.open(AddCardDialogComponent, { disableClose: true });
    dialogRef.afterClosed().subscribe(async (result: ParsedBusinessCardDTO) => {
      const parsed = this.businessCardParser.parseCard(result.content)

      if(parsed){
        // Get the current authenticated user through Amplify api
        const user = await Auth.currentAuthenticatedUser()
        console.log(user)
        if(user)
          parsed.user = user.attributes.email
        console.log(parsed)

        // Add card through AppSync
        await this.api.CreateBusinessCardInfo( parsed )

        // Update webpage
        this.getContacts()
      }
    })
  }

  /**
   * Remove a card from the users storage
   *
   * @param id ID of the card to remove
   */
  async removeCard(id: string) {
    // Remove a cardh through AppSync
    await this.api.DeleteBusinessCardInfo({
      id,
    })

    // Update webpage
    this.getContacts()
  }

  /**
   * Get all the contacts belonging to a user. This wil populate the dashboard.
   */
  async getContacts() {
    const user = await Auth.currentAuthenticatedUser()
    if(user) {
      const result = (await this.api.ListBusinessCardInfos({
        user: {
          eq: user.attributes.email
        },
      }))
      this.cards = result.items.filter(card => card) as BusinessCardInfo[]
    }
  }

  /**
   * Track contacts by the contact ID to prevent unnecessary rerendering.
   */
  trackContacts(index: number, card: BusinessCardInfo) {
    return card.id
  }
}
