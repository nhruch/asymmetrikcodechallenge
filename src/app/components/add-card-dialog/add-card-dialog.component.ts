import { Component, OnInit, HostListener } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AwsService } from '../../services/aws.service';
import { BusinessCardInputType, ParsedBusinessCardDTO } from '../../types/parsed-business-card.dto';
import * as path from 'path'

@Component({
  selector: 'app-add-card-dialog',
  templateUrl: './add-card-dialog.component.html',
  styleUrls: ['./add-card-dialog.component.scss']
})
export class AddCardDialogComponent implements OnInit {
  dragAreaClass = ""
  content = ""
  processing = false
  filename = ""
  type = BusinessCardInputType.MANUAL

  constructor(
    private awsService: AwsService,
    public dialogRef: MatDialogRef<AddCardDialogComponent>
  ) { }

  ngOnInit(): void {
    this.dragAreaClass = "dragarea";
  }

  onFileChange(event: any) {
    let files: FileList = event.target.files;
    this.chooseImage(files);
  }

  @HostListener("dragover", ["$event"]) onDragOver(event: any) {
    this.dragAreaClass = "droparea";
    event.preventDefault();
  }
  @HostListener("dragenter", ["$event"]) onDragEnter(event: any) {
    this.dragAreaClass = "droparea";
    event.preventDefault();
  }
  @HostListener("dragend", ["$event"]) onDragEnd(event: any) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
  }
  @HostListener("dragleave", ["$event"]) onDragLeave(event: any) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
  }
  @HostListener("drop", ["$event"]) async onDrop(event: any) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
    event.stopPropagation();
    if (event.dataTransfer.files) {
      let files: FileList = event.dataTransfer.files;
      await this.chooseImage(files);
    }
  }

   /**
    * Check that an image is valid before processing. Set the processing flag
    * to true while processing occurs.
    *
    * @param files Files dragged onto HTML element
    * @returns
    */
  async chooseImage(files: FileList) {
    if (files.length > 1) {
      console.error("Cannot choose more than one file at a time.")
      return
    }

    const file = files[0]
    this.processing = true
    await this.processImage(file)
    this.processing = false
  }

  /**
   * Submit an image to Textract for OCR processing.
   *
   * @param file File to upload and process
   * @returns
   */
  private async processImage(file: File) {
    this.filename = file.name
    this.content = ""

    // Make sure that the file type is valid
    const file_parts = path.basename(file.name).split('.')
    let ext: string | undefined = ""
    if (file_parts.length > 1) {
      ext = file_parts.pop()
      if (!ext || ext !== "jpg" && ext !== "jpeg" && ext !== "png") {
        console.error("File must be a JPEG image.")
        return
      }
    }

    // Create a more unique key with a timestamp
    const key = [file_parts.join(), "_", (Date.now()/1000).toFixed(0).toString(), ext].join("")

    // Try to upload the file to S3 so that it may later be used in the contact chart.
    try {
      const uploadResult = await this.awsService.uploadImage(file, key)
      console.log(uploadResult)
    } catch (err) {
      console.error("Failed to upload image: ", err)
      return
    }

    // Run OCR on the S3 document and submit the results to be parsed.
    try {
      const ocrResult = await this.awsService.extractDocument(key)
      if(ocrResult?.Blocks) {
        const lines = []
        for(const block of ocrResult.Blocks) {
          // Only take line items right now. Words seem a little too sporadic and inconsisten
          if(block.BlockType === 'LINE') {
            lines.push(block.Text)
          }
        }
        this.content = lines.join("\n")
      }
    } catch (err) {
      console.error("Failed to run text extract: ", err)
      return
    }
  }

  /**
   * Pass the content back to the dashboard on close
   */
  onSubmit() {
    this.dialogRef.close({
      type: this.type,
      content: this.content,
    } as ParsedBusinessCardDTO)
  }
}
