export type AmplifyDependentResourcesAttributes = {
    "auth": {
        "asymmetrikbusinessca1b609628": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string",
            "UserPoolId": "string",
            "UserPoolArn": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string",
            "CreatedSNSRole": "string"
        }
    },
    "api": {
        "asymmetrikbusinessca": {
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    },
    "storage": {
        "UploadImages": {
            "BucketName": "string",
            "Region": "string"
        }
    }
}