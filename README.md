# AsymmetrikBusinessCardApp

This project uses AWS Amplify to deploy a web application to upload and store business card information in the cloud. 

Once a user's account is created and authenticated, they will be given an option to either manually input business card text or provide an image (PNG or JPG) which will be submitted to AWS's OCR service (textra) for text recognition. The resulting text will then be parsed and saved to DocumentDB through an AppSync and GraphQL interface for persistent storage.

The business card parser first looks to find an email address. From that email address, the parser will use the domain as a company hint and the id as a name hint. From there, each line is scanned looking for elements of the business card.

To determine the company and person's name, each line of the card is loaded into a fuzzy hash search engine (fuse.js). Then each hint is broken out into several hints and formatted. For the name hint, remove any qualifiers (e.g. M.D., Jr.), split on spaces, and add the resulting elements to an array of name hints including the joined name. For the company hint, remove the top level domain before scanning. These resulting hints will then scanned against the business card lines looking for strong confidence in similarity. This allows us to more confidently locate the company and person's name on a card. For example:

* 'ABC Tech, Inc' will show high confidence with 'abctech.com' but will fail on all other lines
* 'nhruch' will show high confidence with 'Nicholas Hruch'

Fuzzy hashing and similarity score was chosen because there is so much overlap in structure characteristics of Names, Job Titles, Address strings that it would be very hard to say this definitely looks like a person's name or company. Very often, a person's business card contains an email that has the company's business somewhere in the domain (in some form) and a variation of their own personal name at the front of the email.

For phone numbers, use a very complicated regular expression to identify the phone type, country code, number and extension. The parser supports identifying multiple phone numbers.

For address, start by looking for the "City, State Zip" line via a regular expression. Check to see if the location of any match is greater than 1 which may indicate that the other portions of the address could be before it. Then look one line up to see if the last character of the line ends in a number. If it does, then treat this as Line 2 and use the line above it as Line 1. The exception to this is if the start of the line is a number which indicates that it could include a Line 1. Parse accordingly.

## Getting Started

To try the application, navigate to https://challenge.hruch.com/

You can also run this locally by running `npx ng serve` and navigating to http://localhost:4200

## Things I recognize could have spent more time on...
* Commenting - I did walk through and make comments on the files I felt relevant but I admit I could have added a little more detail. Apologies if something wasn't well explained.
* HTML - Frontend design has not been something I've done a lot with in the past so my UI skills lack. Something I'd like to work on. I'm typically a C++ backend/algorithm developer, done some NodeJS stuff.
* Processing - I realize I went a little past what was asked, but there are still areas where the parsing could be fine tuned. If there is no email, then we don't really have any hints which will cause issues when I try to identify those elements. Also, maybe an email isn't always a good hint... which would then cause someone to fall back on a very bias approach of what cards look like.
